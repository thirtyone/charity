<?php
/*-----------------------------------------------------------------------------------*/
/* This template will be called by all other template files to finish
/* rendering the page and display the footer area/content
/*-----------------------------------------------------------------------------------*/
?>

<footer class="h-10">
<!--    <div class="h-50 bg-white">-->
<!--        <div class="container h-100 pt-3 pb-3">-->
<!--            <div class="row  h-100 align-items-center">-->
<!--                <div class="col-lg-6 col-12 order-lg-1 order-2 text-lg-left  text-center ">-->
<!--                    <ul class="m-0 footer-list pl-0">-->
<!--                        <li>COOKIES</li>-->
<!--                        <li>PRIVACY</li>-->
<!--                        <li>TERM & CONDITION</li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="col-lg-6 col-12 order-lg-2 order-1 text-lg-right  text-center">FOLLOW US ON FACEBOOK</div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <div class="h-100 bg-dark-blue text-white">
        <div class="container h-100">
            <div class="row  h-100 align-items-center">
                <div class="col-lg-6 col-sm-12 text-lg-left  text-center copyright">© 2019 VAPF PH | ALL RIGHTS RESERVED
                </div>
                <div class="col-lg-6 text-right d-lg-block d-sm-none d-none smooth">
<!--                    <a href="#who"><img src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/up.svg"></a>-->


                    <script type="text/javascript"> //<![CDATA[
                        var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.trust-provider.com/" : "http://www.trustlogo.com/");
                        document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
                        //]]></script>
                    <script language="JavaScript" type="text/javascript">
                        TrustLogo("https://www.positivessl.com/images/seals/positivessl_trust_seal_sm_124x32.png", "POSDV", "none");
                    </script>
                </div>
            </div>
        </div>

    </div>
</footer>

</body>
</html>

<?php
/**
 * The template for displaying the home/index page.
 * This template will also be called in any case where the Wordpress engine
 * doesn't know which template to use (e.g. 404 error)
 */

get_header(); ?>

    <section id="who" class="h-remaining primary-section text-white">
        <div class="container h-100">
            <div class="row align-items-lg-center h-100 align-items-end pb-lg-0 pb-5">
                <div class="col-lg-6">
                    <div class="article-object">
<!--                        <p class="article-title h3">WHO WE ARE</p>-->
                        <h4  class="pt-2 article-subtitle">
                            We help Filipinos to reach their full potential as Citizens
                        </h4>
                        <p  class="pt-2 article-content" >The Volunteers Against Poverty Foundation Inc. is a non-stock and non-profit association based in the Philippines which engages in humanitarian activities needed to uplift the welfare, health, and economic conditions of underprivileged Filipino communities</p>

                        <a class="btn-bd-primary">DONATE TODAY</a>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section  class="bg-white text-white">
        <div class="container p-lg-0 p-0 my-sm-4 my-0">
            <div class="row row-eq-height no-gutters my-lg-5 align-items-center bg-dark-blue">
                <div class="col-lg-6 order-lg-1 order-2 img-object">
                    <img class="img-fluid w-100 h-100 img-fit-cover img-position-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/vision.jpg">
                </div>
                <div class="col-lg-6  order-lg-2 order-1 m" id="vision">
                    <div class=" h-100 p-lg-5 px-3 py-5 article-object">
                        <h4  class="pt-2 article-subtitle">Our Vision</h4>
<!--                        <h4  class="pt-2 article-subtitle">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec</h4>-->
                        <p  class="pt-2 article-content" >Volunteers Against Poverty Foundation envisions a better Philippines in which every Filipino family achieves the right to survival through sustainable humanitarian programs.</p>
                    </div>
                </div>
                <div class="col-lg-6  order-lg-3 order-3" id="mission">
                    <div class=" h-100 p-lg-5 px-3 py-5 article-object">

                        <h4  class="pt-2 article-subtitle">Our Mission</h4>
<!--                        <h4 class="pt-2 article-subtitle">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec</h4>-->
                        <p  class="pt-2 article-content">To spread the goodness in volunteerism and how it contributes a significant development not only to the victims of poverty but to everyone who has a heart to give.</p>

                    </div>
                </div>
                <div class="col-lg-6 order-lg-4  order-4 img-object">
                    <img class="img-fluid w-100 h-100 img-fit-cover img-position-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/mission.png"
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="contact-section  text-white">
        <div class="container">

            <div class="row justify-content-center">
<!--                <div class="col-lg-7 ">-->
<!--                    <div class="d-flex h-100 pt-5 pr-5">-->
<!---->
<!--                        <div class="align-self-center article-object">-->
<!--                           <p class="article-title h3"> We’d like to hear from you-->
<!--                           </p>-->
<!--                            <p class="article-content">Get in touch and stay up to date with what’s new-->
<!--                            </p>-->
<!---->
<!--                        </div>-->
<!---->
<!---->
<!--                    </div>-->
<!--                </div>-->
                <div class="col-lg-5">
                    <div class="pt-5 pb-5">
<!--                        <h3 class="text-center">GET IN TOUCH WITH US</h3>-->
                        <div class="text-center article-object">
                            <h4  class="pt-2 article-subtitle"> We’d like to hear from you
                            </h4>
                            <p class="article-content">Get in touch and stay up to date with what’s new
                            </p>

                        </div>
                        <?php echo  do_shortcode( '[contact-form-7 id="8" title="charity"]' ); ?>
<!--                        <form>-->
<!--                            <div class="form-group pt-4 pb-2">-->
<!--                                <input type="text" class="form-control custom-form-control" placeholder="NAME">-->
<!--                            </div>-->
<!--                            <div class="form-group pt-2 pb-2">-->
<!--                                <input type="text" class="form-control custom-form-control" placeholder="EMAIL">-->
<!--                            </div>-->
<!--                            <div class="form-group pt-2 pb-2">-->
<!--                                <textarea  class="form-control custom-form-control" placeholder="MESSAGE"></textarea>-->
<!--                            </div>-->
<!---->
<!--                            <div class="form-group pt-2 pb-2 text-center">-->
<!--                                    <button type="button" class="btn btn-white custom-button custom-button-lg">SUBMIT</button>-->
<!--                            </div>-->


                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>

<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
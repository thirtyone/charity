<?php
/**
 * The template for displaying the home/index page.
 * This template will also be called in any case where the Wordpress engine
 * doesn't know which template to use (e.g. 404 error)
 */

get_header(); ?>

    <section id="who" class="h-remaining primary-section text-white">
        <div class="container h-100">
            <div class="row align-items-lg-center h-100 align-items-end pb-lg-0 pb-5">
                <div class="col-lg-6">
                    <div class="article-object text-md-left  text-center">

                        <h4 class="pt-2 article-subtitle">
                            We help Filipinos to reach their full potential as citizens.
                        </h4>
                        <p class="pt-2 article-content">The Volunteers Against Poverty Foundation Inc. is a non-stock
                            and non-profit association based in the Philippines which engages in humanitarian activities
                            needed to uplift the welfare, health, and economic conditions of underprivileged Filipino
                            communities</p>
                        <a class="btn btn-transparent my-2 px-5 btn-radius-20" data-toggle="modal" data-target="#donateInfo">Donate Today</a>
                    </div>
                </div>

            </div>

        </div>

    </section>

    <section class="bg-white text-white">
        <div class="row no-gutters align-items-center bg-dark-blue">
           
            <div id="vision" class="col-lg-3 img-object">
                <div class=" h-100 ">
                    <img class="img-fluid w-100  img-fit-cover img-position-center"
                         src="<?php echo get_template_directory_uri(); ?>/assets/images/vision.png">
                </div>
            </div>

            <div class="col-lg-3  bg-dark-blue">
                <div class="h-100 text-center px-4">
                    <h4 class="pt-2 article-subtitle">Our Vision</h4>
                    <p class="pt-2 article-content">Volunteers Against Poverty Foundation envisions a better Philippines
                        in which every Filipino family achieves the right to survival through sustainable humanitarian
                        programs.</p>
                </div>
            </div>
            <div id="mission" class="col-lg-3 img-object">
                <div class=" h-100 ">
                    <img class="img-fluid w-100  img-fit-cover img-position-center"
                         src="<?php echo get_template_directory_uri(); ?>/assets/images/mission.png">
                </div>
            </div>

            <div class="col-lg-3  bg-dark-blue">
                <div class="h-100 text-center px-4">
                    <h4 class="pt-2 article-subtitle">Our Mission</h4>
                    <p class="pt-2 article-content">To spread the goodness in volunteerism and how it contributes a
                        significant development not only to the victims of poverty but to everyone who has a heart to
                        give.</p>
                </div>
            </div>
            
        </div>
    </section>
    <!--    <section class="bg-white text-white text-dark pb-5" id="#team">-->
    <!--        <div class="container">-->
    <!--            <h3 class=" text-center py-5">Message from the Board</h3>-->
    <!--            <div class="team-container">-->
    <!--                <div class="row pt-lg-2">-->
    <!--                    <div class="col-xl-6 col-lg-12">-->
    <!--                        <div class="row">-->
    <!--                            <div class="col-lg-6">-->
    <!--                                <img class="img-fluid w-100 img-fit-cover img-position-center"-->
    <!--                                     src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/marcos.png">-->
    <!--                            </div>-->
    <!--                            <div class="col-lg-6">-->
    <!--                                <div class="h5 mb-0 pt-3 pt-lg-0">Francis Leo A. Marcos</div>-->
    <!--                                <small>Chairman</small>-->
    <!--                                <p class="py-4 py-lg-2 article-content">It is our goal is to build a society of braver,-->
    <!--                                    creative, and productive Filipinos. A brighter world filled with dignified new-->
    <!--                                    Filipinos who can withstand any challenge and obstacle that comes their way. It is-->
    <!--                                    my belief that character-building is key to nation-building. Helping our fellow-->
    <!--                                    down-trodden country men is a formidable challenge, but if someone will not take the-->
    <!--                                    lead, then let me take it, as a leader among our peers, to be the first among-->
    <!--                                    equals, and the best among its best.</p>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="col-xl-6 col-lg-12 pt-lg-2">-->
    <!--                        <div class="row">-->
    <!--                            <div class="col-lg-6">-->
    <!--                                <img class="img-fluid w-100 img-fit-cover img-position-center"-->
    <!--                                     src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/bren.png">-->
    <!--                            </div>-->
    <!--                            <div class="col-lg-6">-->
    <!--                                <div class="h5 mb-0 pt-3 pt-lg-0">Bernard Lu Chong</div>-->
    <!--                                <small>President</small>-->
    <!--                                <p class="py-4  py-lg-2 article-content">Helping all our poverty-stricken countrymen is not an-->
    <!--                                    easy task, but if every generous heart will help to this end, no mater how great the-->
    <!--                                    mission is, it will surely be achieved.-->
    <!--                                </p>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!---->
    <!--                </div>-->
    <!--                <div class="row py-lg-3">-->
    <!--                    <div class="col">-->
    <!--                        <div class="card border-0">-->
    <!--                            <img class="card-img-top"-->
    <!--                                 src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/team1.png">-->
    <!--                            <div class="card-body p-0">-->
    <!--                                <div class="h5 pt-3 m-0">Ma. Dayanara Andus</div>-->
    <!--                                <div>Board Member, VAPF, Inc.</div>-->
    <!--                                <p class="card-text py-4">Happiness begins from the moment you do something for others.-->
    <!--                                    All of us can help other people and all can be the part of the mechanism, which can-->
    <!--                                    change our life and make it better. If you help other people, you become happier-->
    <!--                                    than you were before. Always remember, that the purpose of our life is to serve-->
    <!--                                    others.</p>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="col">-->
    <!--                        <div class="card border-0">-->
    <!--                            <img class="card-img-top"-->
    <!--                                 src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/team-placeholder.png">-->
    <!--                            <div class="card-body p-0">-->
    <!--                                <div class="h5 pt-3 m-0">Florencio V. Anchuvas</div>-->
    <!--                                <div>Board Member, VAPF, Inc.</div>-->
    <!--                                <p class="card-text py-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.-->
    <!--                                    Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.-->
    <!--                                    Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="col">-->
    <!--                        <div class="card border-0">-->
    <!--                            <img class="card-img-top"-->
    <!--                                 src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/team-placeholder.png">-->
    <!--                            <div class="card-body p-0">-->
    <!--                                <div class="h5 pt-3 m-0">Ralph P. Purigay</div>-->
    <!--                                <div>Board Member, VAPF, Inc.</div>-->
    <!--                                <p class="card-text py-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.-->
    <!--                                    Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.-->
    <!--                                    Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="col">-->
    <!--                        <div class="card border-0">-->
    <!--                            <img class="card-img-top"-->
    <!--                                 src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/team-placeholder.png">-->
    <!--                            <div class="card-body p-0">-->
    <!--                                <div class="h5 pt-3 m-0">Jefferson John Chua Yujenco</div>-->
    <!--                                <div>Board Member, VAPF, Inc.</div>-->
    <!--                                <p class="card-text py-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.-->
    <!--                                    Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.-->
    <!--                                    Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="col">-->
    <!--                        <div class="card border-0">-->
    <!--                            <img class="card-img-top"-->
    <!--                                 src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/team-placeholder.png">-->
    <!--                            <div class="card-body p-0">-->
    <!--                                <div class="h5 pt-3 m-0">Arnulfo De Leon</div>-->
    <!--                                <div>Board Member, VAPF, Inc.</div>-->
    <!--                                <p class="card-text py-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.-->
    <!--                                    Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis.-->
    <!--                                    Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>-->
    <!---->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!---->
    <!--    </section>-->


    <section class="bg-white text-dark" id="team">
        <div class="container p-lg-0 p-0 my-sm-4 my-0 text-center">
            <h4 class="mt-5 mb-lg-5 mb-0">Our Team</h4>
            <div class="row row-eq-height no-gutters my-lg-5 align-items-center">

                <div class="col-lg-6 mb-lg-4 mb-0 order-lg-1 order-2 img-object">
                    <img class="img-fluid w-100 h-100 img-fit-cover img-position-center"
                         src="<?php echo get_template_directory_uri(); ?>/assets/images/marcos.png">
                </div>
                
                <div class="col-lg-6 mb-2 mb-lg-4 mb-0 order-lg-2 order-1 m" >
                    <div class=" h-100 p-lg-5 px-3 py-4">

                        <p class="h5 font-weight-normal">“Helping our fellow down-trodden country men
                            is a formidable challenge, but if someone will not take the lead, then let me take it, as a
                            leader among our peers, to be the first among equals, and the best among its best.“</p>
                        <div class="h6 pt-3 m-0 font-weight-bold">Francis Leo A. Marcos</div>
                        <span>Chairman</span>


                    </div>
                </div>
                <div class="col-lg-6  order-lg-3 order-3" >
                    <div class=" h-100 p-lg-5 px-3 py-4">
                        <p class="h5 font-weight-normal">“Helping all our poverty-stricken countrymen
                            is not an easy task, but if every generous heart will help to this end, no mater how great
                            the mission is, it will surely be achieved.”</p>
                        <div class="h6 pt-3 m-0 font-weight-bold">Bernard Chong</div>
                        <span>President</span>
                    </div>
                </div>
                
                <div class="col-lg-6 order-lg-4  order-4 img-object">
                    <img class="img-fluid w-100 h-100 img-fit-cover img-position-center"
                         src="<?php echo get_template_directory_uri(); ?>/assets/images/bren.png" />
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="contact-section  text-white">
        <div class="container">

            <div class="row justify-content-center">

                <div class="col-lg-5">
                    <div class="pt-5 pb-5">
                        <!--                        <h3 class="text-center">GET IN TOUCH WITH US</h3>-->
                        <div class="text-center article-object">
                            <h4 class="pt-2 article-subtitle"> We’d like to hear from you
                            </h4>
                            <p class="article-content">Get in touch and stay up to date with what’s new
                            </p>

                        </div>
						<?php echo do_shortcode('[contact-form-7 id="8" title="charity"]'); ?>
                    </div>
                </div>
            </div>
        </div>

    </section>

<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
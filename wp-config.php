<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'charity' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '34$z),XNanf1vuz59,k)n&cwNb,li<-kV~_+8r/%5ng}zYlcT|Jb.qg2ok,dOV~>' );
define( 'SECURE_AUTH_KEY',  '%K4scbo8&k$~Y_czff))y#s:BvSK}hvvDY7F8J2zlAQ@EE*<Yik_U.ZmZp._M{pH' );
define( 'LOGGED_IN_KEY',    'kkqT6^5@&`](k*vt:uK4o~q>bV{L2iM/eb}|:~N:xDI_9f yw&oLpt`$ountYgK^' );
define( 'NONCE_KEY',        '?(0_{Hb-hX=Ob]D`&lDTCXIr[.m-Ziz|vsl.X_xVbt|X$Z=MPefc^27Lfvxij~GK' );
define( 'AUTH_SALT',        'O.%)Z6o}4O$DX0bZ>K:#lYeg&n7Nci$+ty4;h1,ft]YVRn?*Kx/.`hYX&$Qo:w7Y' );
define( 'SECURE_AUTH_SALT', 'i&!K5 lN4~ZamTv24ppT+sZ?;QdPfmzjo`D~4DMREgt=OptL>4e2Ui%Bd@u1nU2R' );
define( 'LOGGED_IN_SALT',   'D~Vw8JNG%.wc+N];[hGs~EaWpG+b=W,9-IZ?cxL8WgAD:kC5mhr7@jb63m`0t6iq' );
define( 'NONCE_SALT',       '4W[B;2)]HjEeRd7~KgFC<O.XcoC:eR#7agw<bzm%Zs%sm#fdHqv4.^Lf]{sv`2])' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
